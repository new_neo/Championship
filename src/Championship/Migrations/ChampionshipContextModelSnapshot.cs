﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Championship.Model;

namespace Championship.Migrations
{
    [DbContext(typeof(ChampionshipContext))]
    partial class ChampionshipContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Championship.Model.Match", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("TeamOneGoal");

                    b.Property<int>("TeamOneId");

                    b.Property<int>("TeamTwoGoal");

                    b.Property<int>("TeamTwoId");

                    b.HasKey("Id");

                    b.HasIndex("TeamOneId");

                    b.HasIndex("TeamTwoId");

                    b.ToTable("Matches");
                });

            modelBuilder.Entity("Championship.Model.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("Teams");
                });

            modelBuilder.Entity("Championship.Model.Match", b =>
                {
                    b.HasOne("Championship.Model.Team", "TeamOne")
                        .WithMany()
                        .HasForeignKey("TeamOneId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Championship.Model.Team", "TeamTwo")
                        .WithMany()
                        .HasForeignKey("TeamTwoId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
