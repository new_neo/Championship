﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Championship.Model;
using Microsoft.EntityFrameworkCore;

namespace Championship.Controllers
{
    public class TeamModel
    {
        public string name { get; set; }
    }
    public class MatchModel
    {
        public int id { get; set; }
        public int idTeamOne { get; set; }
        public int idTeamTwo { get; set; }
        public int countGoalsTeamOne { get; set; }
        public int countGoalsTeamTwo { get; set; }
    }
    public class ApiController : Controller
    {
        private readonly string password = "xxx";

        private readonly ChampionshipContext db;
        public ApiController(ChampionshipContext cc)
        {
            db = cc;
            db.Database.Migrate();
        }

        [HttpPost]
        public IActionResult TeamWrite([FromBody] TeamModel teamModel, [FromHeader(Name = "pas")] string pas)
        {
            if (pas == password)
            {
                //using (var db = new ChampionshipContext())
                {
                    try
                    {
                        db.Teams.Add(new Team() { Name = teamModel.name });
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(ex.Message);
                    }
                }
                return Ok();
            }
            else
            {
                return BadRequest("Доступ запрещён");
            }
        }

        [HttpPost]
        public IActionResult MatchWrite([FromBody] MatchModel matchModel, [FromHeader(Name = "pas")] string pas)
        {
            if (pas == password)
            {
                //using (var db = new ChampionshipContext())
                {
                    try
                    {
                        Match existMatch = db.Matches.Include(m=>m.TeamOne).Include(m=>m.TeamTwo).FirstOrDefault(
                            match => (match.TeamOneId == matchModel.idTeamOne
                                    && match.TeamTwoId == matchModel.idTeamTwo)
                                    ||
                                    (match.TeamOneId == matchModel.idTeamTwo
                                    && match.TeamTwoId == matchModel.idTeamOne)
                                    );

                        if (existMatch != null)
                        {
                            return Ok(existMatch.ToJson());
                        }
                        else
                        {
                            if (matchModel.idTeamOne != matchModel.idTeamTwo)
                            {
                                db.Matches.Add(
                                    new Match()
                                    {
                                        TeamOneId = matchModel.idTeamOne
                                        ,
                                        TeamTwoId = matchModel.idTeamTwo
                                        ,
                                        TeamOneGoal = matchModel?.countGoalsTeamOne ?? 0
                                        ,
                                        TeamTwoGoal = matchModel?.countGoalsTeamTwo ?? 0
                                    }
                                    );
                            }
                            else
                            {
                                return BadRequest("Команда не может играть сама с собой");
                            }
                        }

                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(ex.Message);
                    }
                }
                return Ok();
            }
            else
            {
                return BadRequest("Доступ запрещён");
            }
        }

        [HttpPost]
        public IActionResult MatchUpdate([FromBody] MatchModel matchModel, [FromHeader(Name = "pas")] string pas)
        {
            if (pas == password)
            {
               // using (var db = new ChampionshipContext())
                {
                    try
                    {
                        Match existMatch = db.Matches.FirstOrDefault(
                            match => match.Id == matchModel.id
                                    );

                        if (existMatch != null)
                        {
                            existMatch.TeamOneGoal = matchModel.countGoalsTeamOne;
                            existMatch.TeamTwoGoal = matchModel.countGoalsTeamTwo;
                            existMatch.TeamOneId = matchModel.idTeamOne;
                            existMatch.TeamTwoId = matchModel.idTeamTwo;
                            db.SaveChanges();
                            //return Ok(existMatch);
                        }
                        else
                        {
                            return BadRequest("Такого матча не существует");
                        }                      
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(ex.Message);
                    }
                }
                return Ok();
            }
            else
            {
                return BadRequest("Доступ запрещён");
            }
        }

        [HttpGet]
        public IActionResult GetTeams([FromHeader(Name = "pas")] string pas)
        {
            if (pas == password)
            {
            //    using (var db = new ChampionshipContext())
                {
                    try
                    {
                        List<object> teams = new List<object>();
                        foreach(Team team in db.Teams)
                        {
                            teams.Add(team.ToJson());
                        }
                        return Json(teams);
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(ex.Message);
                    }
                }
            }
            else
            {
                return BadRequest("Доступ запрещён");
            }
        }

        [HttpGet]
        public IActionResult GetMatches([FromHeader(Name = "pas")] string pas)
        {
            if (pas == password)
            {
              //  using (var db = new ChampionshipContext())
                {
                    try
                    {
                        List<object> matches = new List<object>();
                        foreach (Match match in db.Matches.Include((match) =>match.TeamOne).Include((match) => match.TeamTwo))
                        {
                            matches.Add(match.ToJson());
                        }
                        return Json(matches);
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(ex.Message);
                    }
                }
            }
            else
            {
                return BadRequest("Доступ запрещён");
            }
        }


        public IActionResult Index()
        {
            List<TableResult> tableResults = new List<TableResult>();
            List<Team> teams;
            List<Match> matches;
          //  using (var db = new ChampionshipContext())
            {

                teams = db.Teams.ToList();
                matches = db.Matches.Include(match => match.TeamOne).Include(match => match.TeamTwo).ToList();
            }

            foreach(Team team in teams)
            {
                tableResults.Add(new TableResult(team, matches));
            }
                        
            return View(new ModelForView() {
                                            Teams = teams
                                            , TableResults = tableResults
            });
        }

        [HttpGet]
        public IActionResult GetTableCampionship()
        {
            List<TableResult> tableResults = new List<TableResult>();
            List<Team> teams;
            List<Match> matches;
            //  using (var db = new ChampionshipContext())
            {

                teams = db.Teams.ToList();
                matches = db.Matches.Include(match => match.TeamOne).Include(match => match.TeamTwo).ToList();
            }

            foreach (Team team in teams)
            {
                tableResults.Add(new TableResult(team, matches));
            }

          //  var list = new List<object>();
          //  foreach(TableResult tableResult in tableResults)
          //  {
          //      list.Add(tableResult.ToJson());
          //  }
            //var listTeam = new List<Dictionary<string,string>>();
            var listTeam = new List<Dictionary<string, string>>();
            foreach (TableResult row in tableResults)
            {
                var dict = new Dictionary<string, string>();
                dict.Add("0", row.Team.Name);
                int i = 1;
                foreach (TableResult colTeam in tableResults)
                {
                    string result = "X";
                    if(row.Team.Id != colTeam.Team.Id)
                    {
                        var match = row.Matches.Find((item) => item.TeamOneId == colTeam.Team.Id || item.TeamTwoId == colTeam.Team.Id);
                        if(match != null)
                        {
                            if (match.TeamOneId == row.Team.Id)
                                result = match.TeamOneGoal + ":" + match.TeamTwoGoal;
                            if (match.TeamTwoId == row.Team.Id)
                                result = match.TeamTwoGoal + ":" + match.TeamOneGoal;
                        }
                    }
                    dict.Add(i.ToString(), result);
                    i++;
                }

                dict.Add(i.ToString(), row.Win.ToString());
                dict.Add((i+1).ToString(), row.Draw.ToString());
                dict.Add((i+2).ToString(), row.Loss.ToString());

                listTeam.Add(dict);
            }
            return Json(new
            {
                iTotalRecords = 50,
                iTotalDisplayRecords = 10,
                sEcho = 10,
                aaData = listTeam
            });
        }


        public IActionResult Error()
        {
            return View();
        }
    }
}
