﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Championship.Model
{
    public class ChampionshipContext : DbContext
    {
        public DbSet<Team> Teams { get; set; }
        public DbSet<Match> Matches { get; set; }

        //public ChampionshipContext()
        //{

        //}

        public ChampionshipContext(DbContextOptions<ChampionshipContext> options)
            : base(options)
        {
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    //optionsBuilder.UseSqlite("Filename=champ.db");
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>().HasIndex(u => u.Name).IsUnique();
        }
        //public static void Init()
        //{
        //    using (var db = new ChampionshipContext())
        //    {
        //        db.Database.Migrate();

        //        //if (db.Ips.Count() < 1)
        //        //{

        //        //    Country[] countries = new Country[3]
        //        //    {
        //        //   new Country() {Name = "Russia" },
        //        //   new Country() {Name = "USA" },
        //        //   new Country() {Name = "China" }
        //        //    };

        //        //    List<Ip> ipies = new List<Ip>();
        //        //    ipies.Add(new Ip() { Value = "000.000.000.000", Country = countries[0] });
        //        //    ipies.Add(new Ip() { Value = "000.000.000.001", Country = countries[1] });
        //        //    ipies.Add(new Ip() { Value = "000.000.000.002", Country = countries[2] });
        //        //    ipies.Add(new Ip() { Value = "000.000.000.003", Country = countries[0] });
        //        //    ipies.Add(new Ip() { Value = "000.000.000.004", Country = countries[1] });
        //        //    ipies.Add(new Ip() { Value = "000.000.000.005", Country = countries[2] });

        //        //    foreach (Ip ip in ipies)
        //        //    {
        //        //        db.Add(ip);
        //        //    }
        //        //    db.SaveChanges();
        //        //}
        //    }
        //}

    }
}
