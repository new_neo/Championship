﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Championship.Model
{
    public class TableResult
    {
        public readonly Team Team;

        public readonly List<Match> Matches;

       // public int _win;
        public int Win
        {
            get
            {
                return Matches.FindAll(
                    (match) =>
                    {
                        if (Team.Id == match.TeamOne.Id)
                        {
                            return (match.TeamOneGoal > match.TeamTwoGoal);
                        }
                        else
                        {
                            return (match.TeamOneGoal < match.TeamTwoGoal);
                        }
                    }
                    ).Count;
            }
        }
        public int Draw
        {
            get
            {
                return Matches.FindAll(
                    (match) =>
                    {
                        return (match.TeamOneGoal == match.TeamTwoGoal);
                    }
                    ).Count;
            }
        }
        public int Loss
        {
            get
            {
                return Matches.FindAll(
                    (match) =>
                    {
                        if (Team.Id == match.TeamOne.Id)
                        {
                            return (match.TeamOneGoal < match.TeamTwoGoal);
                        }
                        else
                        {
                            return (match.TeamOneGoal > match.TeamTwoGoal);
                        }
                    }
                    ).Count;
            }
        }

        public TableResult(Team team, IEnumerable<Match> matches)
        {
            Team = team;

            Matches = matches.ToList().FindAll(match => match.TeamOne.Id == team.Id || match.TeamTwo.Id == team.Id);
        }

        public object ToJson()
        {
            var list = new List<object>();
            foreach(Match match in Matches)
            {
                list.Add(match.ToJson());
            }

            return new
            {
                team = Team.ToJson()
                , matches = list
                , win = Win
                , draw = Draw
                , loss = Loss
            };
        }
    }
}
