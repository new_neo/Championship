﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Championship.Model
{
    public class ModelForView
    {
        public List<Team> Teams;
        public List<TableResult> TableResults;
    }
}
