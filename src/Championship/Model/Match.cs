﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Championship.Model
{
    public class Match
    {
        public int Id { get; set; }
        public int TeamOneId { get; set; }
        [ForeignKey("TeamOneId")]
        public Team TeamOne { get; set; }
        public int TeamTwoId { get; set; }
        [ForeignKey("TeamTwoId")]
        public Team TeamTwo { get; set; }
        public int TeamOneGoal { get; set; }
        public int TeamTwoGoal { get; set; }

        public object ToJson()
        {
            return new
            {
                id = Id
                ,
                idTeamOne = TeamOne.Id//TeamOne.ToJson()
                ,
                idTeamTwo = TeamTwo.Id //TeamTwo.ToJson()
                ,
                countGoalsTeamOne = TeamOneGoal
                ,
                countGoalsTeamTwo = TeamTwoGoal
            };
        }

        public string ToStringForTeam(Team team)
        {
                string res1 = "";
                string res2 = "";
                if (TeamOne.Id == team.Id)
                {
                    res1 = TeamOneGoal.ToString();
                    res2 = TeamTwoGoal.ToString();
                }
                else
                {
                    res2 = TeamOneGoal.ToString();
                    res1 = TeamTwoGoal.ToString();
                }
                return String.Format("{0}:{1}", res1, res2);
        }
    }
}
