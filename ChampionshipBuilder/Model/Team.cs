﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChampionshipBuilder.Model
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public object ToJson()
        {
            return new
            {
                id = Id
                ,
                name = Name
            };
        }
    }
}
