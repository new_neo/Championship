﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChampionshipBuilder.Model
{
    public class TeamModel
    {
        public int id { get; set; }
        public string name { get;  set;}
    }
}
