﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChampionshipBuilder.Model
{
    public class MatchModel
    {
        public int id { get; set; }
        public int idTeamOne { get; set; }
        public int idTeamTwo { get; set; }
        public int countGoalsTeamOne { get; set; }
        public int countGoalsTeamTwo { get; set; }
    }
}
