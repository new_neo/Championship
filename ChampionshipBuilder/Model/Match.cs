﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChampionshipBuilder.Model
{
    public class Match
    {
        public int Id { get; set; }
        public Team TeamOne { get; set; }
        public Team TeamTwo { get; set; }
        public int TeamOneGoal { get; set; }
        public int TeamTwoGoal { get; set; }

        public object ToJson()
        {
            return new
            {
                id = Id
                ,
                teamOne = TeamOne.ToJson()
                ,
                teamTwo = TeamTwo.ToJson()
                ,
                teamOneGoal = TeamOneGoal
                ,
                teanTwoGoal = TeamTwoGoal
            };
        }
    }
}
