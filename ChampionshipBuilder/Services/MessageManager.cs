﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChampionshipBuilder.Services
{
    public static class MessageManager
    {
        public static void ShowMessageBoxError(string message)
        {
            var result = MessageBox.Show(message, "Ошибка");
        }

        public static void ShowMessageBoxMessage(string message)
        {
            var result = MessageBox.Show(message, "Сообщение");
        }

        public static DialogResult ShowMessegeBoxСonfirmation(string message)
        {
            return MessageBox.Show(message, "Подтверждение", MessageBoxButtons.OKCancel);
        }
    }
}
