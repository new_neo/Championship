﻿using ChampionshipBuilder.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ChampionshipBuilder.Services
{
    public static class ServerManger
    {
        private static readonly string urlIP = "localhost:21531";
        private static readonly string urlDomain = "http://" + urlIP + "/api";
        //headers
        private static readonly KeyValuePair<string, string> pasHeader = new KeyValuePair<string, string>("pas", "xxx");

        public static IEnumerable<Team> GetTeams()
        {
            try
            {
                HttpClient hc = new HttpClient();

                hc.Timeout = new TimeSpan(0, 5, 0);

                hc.DefaultRequestHeaders.Add(pasHeader.Key, pasHeader.Value);

                string uri = urlDomain + "/" + "GetTeams";

                Task<HttpResponseMessage> message = hc.GetAsync(uri);

                if (message.Result != null)
                {
                    if (message.Result.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        var content = message.Result.Content;

                        Task<string> task = content.ReadAsStringAsync();

                        string ser = task.Result;

                        IEnumerable<TeamModel> teamsModel = JsonConvert.DeserializeObject<IEnumerable<TeamModel>>(ser);

                        List<Team> listTeam = new List<Team>();
                        foreach (TeamModel teamModel in teamsModel)
                        {
                            listTeam.Add(SerializationModel.GetTeam(teamModel));
                        }

                        return listTeam;
                    }
                    else
                    {
                        MessageManager.ShowMessageBoxError(message.Result.StatusCode + " : " + message.Result.Content.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageManager.ShowMessageBoxError(ex.Message + " : " + ex.InnerException);
            }
            return null;

        }


        public static IEnumerable<Match> GetMatches()
        {
            try
            {
                HttpClient hc = new HttpClient();

                hc.Timeout = new TimeSpan(0, 5, 0);

                hc.DefaultRequestHeaders.Add(pasHeader.Key, pasHeader.Value);

                string uri = urlDomain + "/" + "GetMatches";

                Task<HttpResponseMessage> message = hc.GetAsync(uri);

                if (message.Result != null)
                {
                    if (message.Result.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        var content = message.Result.Content;

                        Task<string> task = content.ReadAsStringAsync();

                        string ser = task.Result;

                        IEnumerable<MatchModel> matchesModel = JsonConvert.DeserializeObject<IEnumerable<MatchModel>>(ser);

                        List<Match> listMatch = new List<Match>();
                        List<Team> teams = GetTeams().ToList();

                        foreach (MatchModel matchModel in matchesModel)
                        {
                            var match = SerializationModel.GetMatch(matchModel);
                            match.TeamOne = teams.Find(team => team.Id == matchModel.idTeamOne);
                            match.TeamTwo = teams.Find(team => team.Id == matchModel.idTeamTwo);
                            listMatch.Add(match);
                        }

                        return listMatch;
                    }
                    else
                    {
                        MessageManager.ShowMessageBoxError(message.Result.StatusCode + " : " + message.Result.Content.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageManager.ShowMessageBoxError(ex.Message + " : " + ex.InnerException);
            }
            return null;

        }

        public static bool CreateTeam(Team team)
        {
            try
            {
                HttpClient hc = new HttpClient();

                hc.Timeout = new TimeSpan(0, 5, 0);

                hc.DefaultRequestHeaders.Add(pasHeader.Key, pasHeader.Value);

                string uri = urlDomain + "/" + "TeamWrite";

                var contentPost = new StringContent(
                    JsonConvert.SerializeObject(SerializationModel.GetTeamModel(team)).ToString()
                    , Encoding.UTF8
                    , "application/json");

                Task<HttpResponseMessage> message = hc.PostAsync(uri, contentPost);

                if (message.Result != null)
                {
                    if (message.Result.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        var content = message.Result.Content;

                        Task<string> task = content.ReadAsStringAsync();

                        string ser = task.Result;

                        return true;
                    }
                    else
                    {
                        MessageManager.ShowMessageBoxError(message.Result.StatusCode + " : " + message.Result.Content.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageManager.ShowMessageBoxError(ex.Message + " : " + ex.InnerException);
            }
            return false;

        }

        public static bool AddMatch(Match match)
        {
            try
            {
                HttpClient hc = new HttpClient();

                hc.Timeout = new TimeSpan(0, 5, 0);

                hc.DefaultRequestHeaders.Add(pasHeader.Key, pasHeader.Value);

                string uri = urlDomain + "/" + "MatchWrite";

                var contentPost = new StringContent(
                    JsonConvert.SerializeObject(SerializationModel.GetMatchModel(match)).ToString()
                    , Encoding.UTF8
                    , "application/json");

                Task<HttpResponseMessage> message = hc.PostAsync(uri, contentPost);

                if (message.Result != null)
                {
                    if (message.Result.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        var content = message.Result.Content;

                        Task<string> task = content.ReadAsStringAsync();

                        string ser = task.Result;

                        List<Team> teams = GetTeams().ToList();

                        MatchModel matchModel = JsonConvert.DeserializeObject<MatchModel>(ser);
                        if(matchModel != null)
                        {
                           if( MessageManager.ShowMessegeBoxСonfirmation(String.Format(@"В базе данных уже есть матч: {0}:{1}, счет: {2}:{3}
                                Хотите ли вы перезаписать этот матч с такими данными: {4}:{5}, счет: {6}:{7}?"
                                , teams.Find(team => team.Id == matchModel.idTeamOne).Name
                                , teams.Find(team => team.Id == matchModel.idTeamTwo).Name
                                , matchModel.countGoalsTeamOne
                                , matchModel.countGoalsTeamTwo
                                , match.TeamOne.Name, match.TeamTwo.Name, match.TeamOneGoal, match.TeamTwoGoal)) == System.Windows.Forms.DialogResult.OK)
                            {
                                //if(matchModel.idTeamOne == match.TeamOne.Id)
                                //{
                                //    match.TeamOneGoal = matchModel.countGoalsTeamOne;
                                //    match.TeamTwoGoal = matchModel.countGoalsTeamTwo;
                                //}
                                //else
                                //{
                                //    match.TeamOneGoal = matchModel.countGoalsTeamTwo;
                                //    match.TeamTwoGoal = matchModel.countGoalsTeamOne;
                                //}
                                match.Id = matchModel.id;

                                return UpdateMatch(match);
                            }
                           else
                            {
                                return false;
                            }
                        }
                        return true;
                    }
                    else
                    {
                        MessageManager.ShowMessageBoxError(message.Result.StatusCode + " : " + message.Result.Content.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageManager.ShowMessageBoxError(ex.Message + " : " + ex.InnerException);
            }
            return false;

        }

        public static bool UpdateMatch(Match match)
        {
            try
            {
                HttpClient hc = new HttpClient();

                hc.Timeout = new TimeSpan(0, 5, 0);

                hc.DefaultRequestHeaders.Add(pasHeader.Key, pasHeader.Value);

                string uri = urlDomain + "/" + "MatchUpdate";

                var contentPost = new StringContent(
                    JsonConvert.SerializeObject(SerializationModel.GetMatchModel(match)).ToString()
                    , Encoding.UTF8
                    , "application/json");

                Task<HttpResponseMessage> message = hc.PostAsync(uri, contentPost);

                if (message.Result != null)
                {
                    if (message.Result.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        var content = message.Result.Content;

                        Task<string> task = content.ReadAsStringAsync();

                        string ser = task.Result;
                                               
                        return true;
                    }
                    else
                    {
                        MessageManager.ShowMessageBoxError(message.Result.StatusCode + " : " + message.Result.Content.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageManager.ShowMessageBoxError(ex.Message + " : " + ex.InnerException);
            }
            return false;

        }
    }
}
