﻿using ChampionshipBuilder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChampionshipBuilder.Services
{
    public static class SerializationModel
    {
        public static Team GetTeam(TeamModel teamModel)
        {
            Team team = new Team()
            {
                Id = teamModel.id,
                Name = teamModel.name,
                //   Name = cityModel.name
            };

            return team;
        }

        public static Match GetMatch(MatchModel matchModel)
        {
            Match match = new Match()
            {
                Id = matchModel.id
                //, TeamOne = GetTeam(matchModel.idTeamOne)
                //, TeamTwo = GetTeam(matchModel.teamTwo)
                , TeamOneGoal = matchModel.countGoalsTeamOne
                , TeamTwoGoal = matchModel.countGoalsTeamTwo
                //   Name = cityModel.name
            };

            return match;
        }

        public static TeamModel GetTeamModel(Team team)
        {
            TeamModel teamModel = new TeamModel()
            {
                id = team.Id
                , name = team.Name
            };
            return teamModel;
        }

        public static MatchModel GetMatchModel(Match match)
        {
            MatchModel matchModel = new MatchModel()
            {
                id = match.Id
                ,
                idTeamOne = match.TeamOne.Id
                ,
                idTeamTwo = match.TeamTwo.Id
                ,
                countGoalsTeamOne = match.TeamOneGoal
                ,
                countGoalsTeamTwo = match.TeamTwoGoal
            };
            return matchModel;
        }
    }
}
