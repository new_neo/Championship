﻿namespace ChampionshipBuilder
{
    partial class ChampionshipBuilder
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_add_team = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_adding_name_team = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_team1 = new System.Windows.Forms.ComboBox();
            this.comboBox_team2 = new System.Windows.Forms.ComboBox();
            this.numericUpDown_goal_team1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_goal_team2 = new System.Windows.Forms.NumericUpDown();
            this.button_add_match = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_goal_team1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_goal_team2)).BeginInit();
            this.SuspendLayout();
            // 
            // button_add_team
            // 
            this.button_add_team.Location = new System.Drawing.Point(122, 33);
            this.button_add_team.Name = "button_add_team";
            this.button_add_team.Size = new System.Drawing.Size(75, 23);
            this.button_add_team.TabIndex = 0;
            this.button_add_team.Text = "Добавить";
            this.button_add_team.UseVisualStyleBackColor = true;
            this.button_add_team.Click += new System.EventHandler(this.button_add_team_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Имя команды";
            // 
            // textBox_adding_name_team
            // 
            this.textBox_adding_name_team.Location = new System.Drawing.Point(16, 35);
            this.textBox_adding_name_team.Name = "textBox_adding_name_team";
            this.textBox_adding_name_team.Size = new System.Drawing.Size(100, 20);
            this.textBox_adding_name_team.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ввод результатов матчей";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Счет";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Команда 1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(162, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Команда 2";
            // 
            // comboBox_team1
            // 
            this.comboBox_team1.FormattingEnabled = true;
            this.comboBox_team1.Location = new System.Drawing.Point(16, 130);
            this.comboBox_team1.Name = "comboBox_team1";
            this.comboBox_team1.Size = new System.Drawing.Size(121, 21);
            this.comboBox_team1.TabIndex = 7;
            this.comboBox_team1.SelectedIndexChanged += new System.EventHandler(this.comboBox_team1_SelectedIndexChanged);
            // 
            // comboBox_team2
            // 
            this.comboBox_team2.FormattingEnabled = true;
            this.comboBox_team2.Location = new System.Drawing.Point(165, 130);
            this.comboBox_team2.Name = "comboBox_team2";
            this.comboBox_team2.Size = new System.Drawing.Size(121, 21);
            this.comboBox_team2.TabIndex = 8;
            this.comboBox_team2.SelectedIndexChanged += new System.EventHandler(this.comboBox_team2_SelectedIndexChanged);
            // 
            // numericUpDown_goal_team1
            // 
            this.numericUpDown_goal_team1.Location = new System.Drawing.Point(16, 193);
            this.numericUpDown_goal_team1.Name = "numericUpDown_goal_team1";
            this.numericUpDown_goal_team1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_goal_team1.TabIndex = 9;
            // 
            // numericUpDown_goal_team2
            // 
            this.numericUpDown_goal_team2.Location = new System.Drawing.Point(165, 193);
            this.numericUpDown_goal_team2.Name = "numericUpDown_goal_team2";
            this.numericUpDown_goal_team2.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_goal_team2.TabIndex = 10;
            // 
            // button_add_match
            // 
            this.button_add_match.Location = new System.Drawing.Point(16, 231);
            this.button_add_match.Name = "button_add_match";
            this.button_add_match.Size = new System.Drawing.Size(75, 23);
            this.button_add_match.TabIndex = 11;
            this.button_add_match.Text = "Добавить";
            this.button_add_match.UseVisualStyleBackColor = true;
            this.button_add_match.Click += new System.EventHandler(this.button_add_match_Click);
            // 
            // ChampionshipBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 308);
            this.Controls.Add(this.button_add_match);
            this.Controls.Add(this.numericUpDown_goal_team2);
            this.Controls.Add(this.numericUpDown_goal_team1);
            this.Controls.Add(this.comboBox_team2);
            this.Controls.Add(this.comboBox_team1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_adding_name_team);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_add_team);
            this.Name = "ChampionshipBuilder";
            this.Text = "ChampionshipBuilder";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_goal_team1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_goal_team2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_add_team;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_adding_name_team;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox_team1;
        private System.Windows.Forms.ComboBox comboBox_team2;
        private System.Windows.Forms.NumericUpDown numericUpDown_goal_team1;
        private System.Windows.Forms.NumericUpDown numericUpDown_goal_team2;
        private System.Windows.Forms.Button button_add_match;
    }
}

