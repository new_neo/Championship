﻿using ChampionshipBuilder.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChampionshipBuilder
{
    public partial class ChampionshipBuilder : Form
    {
        private List<Team> listTeam = new List<Team>();
        // private List<> 

      //  private List<Team> listForComboboxTeam1 = new List<Team>();
     //   private List<Team> listForComboboxTeam2 = new List<Team>();

        public ChampionshipBuilder()
        {
            InitializeComponent();

            listTeam = Services.ServerManger.GetTeams().ToList();

            //comboBox_team1.DataSource = listForComboboxTeam1;
            //comboBox_team2.DataSource = listForComboboxTeam2;
           // comboBox_team1.Items.AddRange(listForComboboxTeam1.ToArray());
           // comboBox_team2.Items.AddRange(listForComboboxTeam2.ToArray());


            comboBox_team1.DisplayMember = "Name";
            comboBox_team2.DisplayMember = "Name";

            UpdateComboboxes();
            //UpdateComboboxes2();
        }

        private void UpdateComboboxes2()
        {
         //   listForComboboxTeam2.Clear();
            
            var selectedItem = comboBox_team2.SelectedItem;
            comboBox_team2.Items.Clear();

            var list = listTeam.FindAll(
                (item) =>
                item.Name != ((Team)comboBox_team1.SelectedItem)?.Name
                );
         //   listForComboboxTeam2.AddRange(list);

        //    comboBox_team2.Items.AddRange(listForComboboxTeam2.ToArray());
            comboBox_team2.Update();

            comboBox_team2.SelectedItem = selectedItem;
        }

        private void UpdateComboboxes1()
        {
       //     listForComboboxTeam1.Clear();

            var selectedItem = comboBox_team1.SelectedItem;
            comboBox_team1.Items.Clear();

       //     listForComboboxTeam1.AddRange(listTeam.FindAll((item) => item.Name != ((Team)comboBox_team2.SelectedItem)?.Name));
       //     comboBox_team1.Items.AddRange(listForComboboxTeam1.ToArray());
            comboBox_team1.Update();

            comboBox_team1.SelectedItem = selectedItem;
        }

        private void UpdateComboboxes()
        {
            comboBox_team1.Items.Clear();
            comboBox_team2.Items.Clear();

            comboBox_team1.Items.AddRange(
                listTeam.ToArray()
                    );

            comboBox_team2.Items.AddRange(
                listTeam.ToArray()
                    );

            comboBox_team1.Update();
            comboBox_team2.Update();
        }

        private void button_add_team_Click(object sender, EventArgs e)
        {
            if (Services.ServerManger.CreateTeam(new Team() { Name = textBox_adding_name_team.Text }))
            {
                listTeam = Services.ServerManger.GetTeams().ToList();
                UpdateComboboxes();
            }
            textBox_adding_name_team.Clear();
        }

        private void comboBox_team1_SelectedIndexChanged(object sender, EventArgs e)
        {
           // UpdateComboboxes();
        }

        private void comboBox_team2_SelectedIndexChanged(object sender, EventArgs e)
        {
           // UpdateComboboxes();
        }

        private void button_add_match_Click(object sender, EventArgs e)
        {
            if (comboBox_team1.SelectedIndex == -1)
            {
                Services.MessageManager.ShowMessageBoxError("Выберете команду №1");
                return;
            }
            if (comboBox_team2.SelectedIndex == -1)
            {
                Services.MessageManager.ShowMessageBoxError("Выберете команду №2");
                return;
            }

            if(Services.ServerManger.AddMatch(
                new Match()
                {
                    TeamOne = (Team)comboBox_team1.SelectedItem
                    ,
                    TeamTwo = (Team)comboBox_team2.SelectedItem
                    , TeamOneGoal = (int)numericUpDown_goal_team1.Value
                    , TeamTwoGoal = (int)numericUpDown_goal_team2.Value
                }
                ))
            {
                comboBox_team1.SelectedIndex = -1;
                comboBox_team2.SelectedIndex = -1;
            }
        }
    }
}
